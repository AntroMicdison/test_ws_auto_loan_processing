package com.ezb.alp.service;

import com.ezb.alp.Application
import com.ezb.alp.User
import com.ezb.alp.model.LoanApplication
import com.ezb.alp.repository.ApplicationRepository
import com.ezb.alp.repository.UserRepository
import static org.junit.Assert.*;
import spock.lang.Specification

/**
 * TDD test using Spock Framework
 * Class used to test LoanApplicationServiceGroovyTest methods
 * @author Jerry Rydere 
 *
 */
class LoanApplicationServiceTest  extends Specification {

	/**
	 * Application Service Declaration
	 */
	private LoanApplicationService loanApplicationService;
	
	/**
	 * LoanApplication VO Declaration
	 */
	private LoanApplication loanApplication;
	
	/**
	 * ApplicationRepository Declaration
	 */
	private ApplicationRepository repository;
	
	/**
	 * UserRepository Declaration
	 */
	private UserRepository userRepository;

	/**
	 * Application Entity Declaration
	 */
	private Application application;

	/**
	 * User Entity Declaration
	 */
	private User user;
	

	/**
	 * Setting the User, Application, Branch, VehicleDetail entity & LoanApplication values across the class
	 * @return
	 */
	def setup() {
		application = new Application();
		loanApplication = new LoanApplication();
		user = new User();
		application.setApplicationId("20160800000001");
		user.setUserName("Jerry");
		loanApplication.setUser(user);
	}
	
	
	def 'validate account number generation logic'() {
		given:"Validating Account Number Generation Code"
			LoanApplicationService loanApplicationService1 = new LoanApplicationService();
			String accntNo = "20160800000001";
			String expectedAccntNo = "20160800000002";
		when:"Check The Account Number Generation Logic"
			if(accntNo!=null && accntNo!=" "){
				loanApplicationService1.generateAccountNumber(accntNo);
			}
		then:"Generated the New ApplicationId"
			loanApplicationService1.generateAccountNumber(accntNo)  >>  expectedAccntNo;
	}

	def 'save loanApplicationDetail by given loanApplication value object'() {
		given:"Save The Loan Application Data"
			userRepository = Mock();
			user = new User();
			user.setUserName("Jerry");
			loanApplication = new LoanApplication();
			loanApplication.setUser(user);
			application = new Application();
			application.setApplicationId("20160800000001");
			loanApplication.setApplication(application);
		when :"Final Insertion of application"
			 loanApplication.setUser(user);
			 repository = Mock();
			 loanApplicationService = new LoanApplicationService(repository:repository);
			 repository.findTopByOrderByApplicationIdDesc() >> application ;
			 userRepository = Mock();
			 loanApplicationService = new LoanApplicationService(userRepository:userRepository);
			 userRepository.insert(loanApplication.getUser()) >> user ;
			 loanApplicationService.saveLoanApplicationDetails(loanApplication);
		then:"Returns Application Object"
			thrown(Exception)
			repository.insert(application) >> application;
				
	}
	
	def 'save loanApplicationDetail by passing null object'() {
		given:"Save The Loan Application Data"
			loanApplication = null;
			repository = Mock();
			loanApplicationService = new LoanApplicationService(repository:repository);
		when:"Check The LoanApplication Value Object is Not Null"
			if(loanApplication!=null){
				repository.findTopByOrderByApplicationIdDesc();
			}	
		then:"LoanApplication is Found As NULL Object"
			loanApplicationService.saveLoanApplicationDetails(loanApplication)  >>  application;
			assertNull(loanApplication);
	}
	
	def 'check account number is empty or null'() {
		given:"Get The Max Account Number For Inserting Record"
			loanApplicationService = Mock(LoanApplicationService);
			String accntNo = " ";
		when:"Check The Account Number is Empty or Null"
			if(accntNo!=" "){
				loanApplicationService.generateAccountNumber(accntNo);
			}
		then:"Throw Exception Account Number is Empty or Null"
			//loanApplicationService.generateAccountNumber(accntNo)  >>  accntNo;
	}
	
	
}
