package com.ezb.alp.controller

import spock.lang.Ignore;
import spock.lang.Specification

import com.ezb.alp.Application
import com.ezb.alp.Branch
import com.ezb.alp.User
import com.ezb.alp.VehicleDetail
import com.ezb.alp.model.LoanApplication
import com.ezb.alp.repository.ApplicationRepository;
import com.ezb.alp.repository.BranchRepository
import com.ezb.alp.repository.VehicleDetailRepository
import com.ezb.alp.service.LoanApplicationService

class LoanApplicationControllerTest  extends Specification {
	
	
	/**
	 * LoanApplicationController Declaration
	 */
	private LoanApplicationController loanApplicationController;
	
	/**
	 * LoanApplicationController Declaration
	 */
	private LoanApplicationController loanApplVehicleController;
	
	/**
	 * LoanApplicationService Declaration
	 */
	private LoanApplicationService loanApplicationService;
	
	/**
	 * BranchRepository Declaration
	 */
	private BranchRepository branchRepository;
	
	/**
	 * VehicleDetailRepository Declaration
	 */
	private VehicleDetailRepository vehicleDetailsRepository
	
	/**
	 * ApplicationRepository Declaration
	 */
	private ApplicationRepository applicationRepository;
	
	/**
	 * Application Entity Declaration
	 */
	private Application application;
	
	/**
	 * Branch Entity Declaration
	 */
	private Branch branch;
	
	/**
	 * User Entity Declaration
	 */
	private User user;
	
	/**
	 * VehicleDetail Entity Declaration
	 */
	private List<VehicleDetail> vehicleDetail;
	
	/**
	 * LoanApplication VO Declaration
	 */
	private LoanApplication loanApplication;
	
	
	/**
	 * Setting the User entity values across the class
	 * @return
	 */
	def setup() {
		application = new Application();
		application.setApplicationId("20160800000001");
		application.setRequestedAmt(250000);
		application.setLoanTerm(3);
		loanApplication = new LoanApplication();
		user = new User();
		user.setUserName("Jerry");
		loanApplication.setUser(user);
		branch = new Branch();
		vehicleDetail = new ArrayList<VehicleDetail>();
		
		
	}
	
	def 'save loanApplicationDetail by given loanApplication value object'() {
		given:"Save The Loan Application Data"
			loanApplicationService = Mock();
			loanApplicationController = new LoanApplicationController(loanApplicationService:loanApplicationService);
			LoanApplication loanAppl = Mock();
		when:"Set The LoanApplication Value Object and Pass It As Parameter"
			loanApplicationController.saveLoanApplicationDetails(loanAppl);
		then:"Persist The Record"
			 loanApplicationService.saveLoanApplicationDetails(loanAppl) >>  application;
	}
	
	def 'get branch details based on zipcode'() {
		given:"To Get Branch Details"
			String zipCode = "01890";
			branchRepository = Mock();
			loanApplicationController = new LoanApplicationController(branchRepository:branchRepository);
		when:"Get Branch Details Based On Zip Code"
			if(zipCode!=null && zipCode!=" "){
				loanApplicationController.fetchBranchDetails(zipCode);
			}
		then:"Generated Branch Details"
			 branchRepository.findByZipCode(zipCode) >> branch;
	}
	
	def 'validation for zipcode'() {
		given:"To Validate The Zip Code"
			String zipCode = "01890";
			branchRepository = Mock();
		when:"Check The Zip Code is Not Empty And Not Null"
			if(zipCode!=null && zipCode!=" "){
				branchRepository.findByZipCode(zipCode);
			}
		then:"Throw Exception Zip Code is Empty or Null"
	}
	
	def 'get vehicle details based on vehicle make'() {
		given:"To Get Vehicle Details"
			String vehicleMake = "GM";
			vehicleDetailsRepository = Mock();
			loanApplVehicleController = new LoanApplicationController(vehicleDetailsRepository:vehicleDetailsRepository);
		when:"Get Vehicle Details Based On Vehicle Make"
			if(vehicleMake!=null && vehicleMake!=" "){
				loanApplVehicleController.findByVehicleModel(vehicleMake)
			}
		then:"Generated Vehicle Details"
			vehicleDetailsRepository.findByVehicleMake(vehicleMake) >> vehicleDetail;
	}
	
	def 'validation for vehicle make'() {
		given:"To Validate The Vehicle Make"
			//loanApplicationController = Mock(LoanApplicationController);
			vehicleDetailsRepository = Mock();
			String vehicleMake = " ";
		when:"Check The Vehicle Make is Not Empty And Not Null"
			if(vehicleMake!=null && vehicleMake!=" "){
				vehicleDetailsRepository.findByVehicleMake(vehicleMake);
			}
		then:"Throw Exception Vehicle Make is Empty or Null"
	}
	
	def 'get application id based on user name'() {
		given:"To Get ApplicationId"
			String userName = "Jerry";
			applicationRepository = Mock();
			loanApplicationController = new LoanApplicationController(applicationRepository:applicationRepository);
		when:"Get Application Id Based On userName"
			if(userName!=null && userName!=" "){
				loanApplicationController.findByUserName(userName)
			}
		then:"Retrieval of ApplicationId"
			applicationRepository.findByUserName(userName) >> application;
	}
	
	

}
