package com.ezb.alp.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.ezb.alp.Application;
import com.ezb.alp.User;
import com.ezb.alp.model.LoanApplication;
import com.ezb.alp.repository.ApplicationRepository;
import com.ezb.alp.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class LoanApplicationServiceTesting {

	@Mock
	private ApplicationRepository repository;

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private LoanApplicationService loanApplicationService;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testSaveLoanApplicationDetails() throws Exception {
		User user = new User();
		user.setUserName("john");

		Application application = new Application();
		application.setApplicationId("201610000001");
		application.setUser(user);

		LoanApplication loanApplication = new LoanApplication();
		loanApplication.setUser(user);
		loanApplication.setApplication(application);

		Mockito.when(userRepository.insert(loanApplication.getUser())).thenReturn(user);
		Mockito.when(repository.findTopByOrderByApplicationIdDesc()).thenReturn(application);
		Mockito.when(repository.insert(application)).thenReturn(application);
		Mockito.when(loanApplicationService.saveLoanApplicationDetails(loanApplication)).thenReturn(application);
	}
	
	@Test
	public void testSaveLoanApplicationDetailsNullValue() throws Exception {
		User user = new User();
		user.setUserName("john");

		Application application = new Application();
		application.setApplicationId(null);
		application.setUser(user);

		LoanApplication loanApplication = new LoanApplication();
		loanApplication.setUser(user);
		loanApplication.setApplication(application);

		Mockito.when(userRepository.insert(loanApplication.getUser())).thenReturn(user);
		Mockito.when(repository.findTopByOrderByApplicationIdDesc()).thenReturn(application);
		Mockito.when(repository.insert(application)).thenReturn(application);
		Mockito.when(loanApplicationService.saveLoanApplicationDetails(loanApplication)).thenReturn(application);
	}

}
