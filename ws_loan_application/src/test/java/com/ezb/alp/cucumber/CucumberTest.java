package com.ezb.alp.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources", tags = { "~@wip, @executeThis" }, monochrome = true, plugin = {
		"pretty", "html:build/cucumber", "json:build/cucumber.json" })
public class CucumberTest {

}
