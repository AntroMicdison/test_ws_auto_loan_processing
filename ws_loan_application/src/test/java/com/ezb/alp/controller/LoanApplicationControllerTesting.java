package com.ezb.alp.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.ezb.alp.Application;
import com.ezb.alp.Branch;
import com.ezb.alp.VehicleDetail;
import com.ezb.alp.model.LoanApplication;
import com.ezb.alp.repository.ApplicationRepository;
import com.ezb.alp.repository.BranchRepository;
import com.ezb.alp.repository.VehicleDetailRepository;
import com.ezb.alp.service.LoanApplicationService;

import gherkin.deps.com.google.gson.Gson;

public class LoanApplicationControllerTesting {

	private MockMvc mockMvc;

	@Mock
	private LoanApplicationService loanApplicationService;

	@Mock
	@Autowired
	private BranchRepository branchRepository;

	@Mock
	@Autowired
	private ApplicationRepository applicationRepository;

	@Mock
	@Autowired
	private VehicleDetailRepository vehicleDetailsRepository;

	@InjectMocks
	private LoanApplicationController loanApplicationController;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(loanApplicationController).build();
	}

	@Test
	public void testSaveLoanApplicationDetails() throws Exception {
		LoanApplication loanApplication = new LoanApplication();
		Application app = new Application();
		app.setRequestedAmt(35000);
		Mockito.when(loanApplicationService.saveLoanApplicationDetails(any(LoanApplication.class))).thenReturn(app);
		Gson gson = new Gson();
		String json = gson.toJson(loanApplication);
		mockMvc.perform(post("/loanapplication").contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.content(json)).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(MockMvcResultMatchers.jsonPath("requestedAmt", is(35000)));
	}

	@Test
	public void testFindByVehicleModel() throws Exception {
		Application app = new Application();
		app.setRequestedAmt(35000);
		VehicleDetail vehicleDetail = new VehicleDetail();
		vehicleDetail.setVehicleMake("Honda");
		List<VehicleDetail> vehicleList = new ArrayList<VehicleDetail>();
		vehicleList.add(vehicleDetail);
		Mockito.when(vehicleDetailsRepository.findByVehicleMake(vehicleDetail.getVehicleMake()))
				.thenReturn(vehicleList);
		mockMvc.perform(
				get("/vehicleDetails?vehicleMake=" + vehicleDetail.getVehicleMake()).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void testFindByUserName() throws Exception {
		String userName = "john";
		Application app = new Application();
		app.setApplicationId("786");
		app.setRequestedAmt(35000);
		Mockito.when(applicationRepository.findByUserName(userName)).thenReturn(app);
		mockMvc.perform(get("/username?userName=" + userName).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void testFetchBranchDetails() throws Exception {
		String zipCode = "123456";
		Branch branch = new Branch();
		branch.setBranchName("New Jersy");
		Mockito.when(branchRepository.findByZipCode(zipCode)).thenReturn(branch);
		mockMvc.perform(get("/branchdetail?zipCode=" + zipCode).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
}
