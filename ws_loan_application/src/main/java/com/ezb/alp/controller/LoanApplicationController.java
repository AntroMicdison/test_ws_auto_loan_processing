package com.ezb.alp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ezb.alp.Application;
import com.ezb.alp.Branch;
import com.ezb.alp.VehicleDetail;
import com.ezb.alp.model.LoanApplication;
import com.ezb.alp.repository.ApplicationRepository;
import com.ezb.alp.repository.BranchRepository;
import com.ezb.alp.repository.VehicleDetailRepository;
import com.ezb.alp.service.LoanApplicationService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/")
public class LoanApplicationController {

	private Logger logger = LoggerFactory.getLogger(LoanApplicationController.class);

	@Autowired
	private LoanApplicationService loanApplicationService;
	
	@Autowired
	private BranchRepository branchRepository;
	
	@Autowired
	private ApplicationRepository applicationRepository;
	
	@Autowired
	private VehicleDetailRepository vehicleDetailsRepository;

	/**
	 * Save Application Details
	 * 
	 * @param application
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(
		    value = "Submit Loan Application",
		    notes =
		        "Service for Create and Submit a Loan Applicaiton Details",
		    response = Application.class)
	@RequestMapping(value = "/loanapplication", method = RequestMethod.POST)
	public @ResponseBody Application saveLoanApplicationDetails(@ApiParam(value = "Loan Application Details", required = true) @RequestBody LoanApplication loanApplication) throws Exception {
		Application app = loanApplicationService.saveLoanApplicationDetails(loanApplication);
		return app;
	}
	
	/**
	 * Service get the Branch Details based on the given Zipcode
	 * 
	 * @param zipCode
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(
		    value = "Fetch Loan Branch Details",
		    notes =
		        "Service for fetch the Branch Details based on the Given Branch Zip code",
		    response = Branch.class)
	@RequestMapping(value = "/branchdetail", method = RequestMethod.GET)
	public Branch fetchBranchDetails(@ApiParam(value = "Zipcode", required = true) @RequestParam String zipCode) throws Exception {
		return branchRepository.findByZipCode(zipCode);	
	}
	
	/**
	 * Service for get the List of Vehicle Details
	 * 
	 * @param vehicleMake
	 * @return
	 * @throws Exception
	 */	
	@ApiOperation(
		    value = "Find Vehicle Detail",
		    notes =
		        "Service for find vehicle model based on vehicle make", response = VehicleDetail.class, responseContainer = "List")
	@RequestMapping(value = "/vehicleDetails", method = RequestMethod.GET)
	public @ResponseBody List<VehicleDetail> findByVehicleModel(@ApiParam(value = "vehicleMake", required = true) @RequestParam String vehicleMake) throws Exception {
		logger.info("Entered findVehicleDetail()");
		return vehicleDetailsRepository.findByVehicleMake(vehicleMake);
	}
	
	
	/**
	 * Service for getting Application Id
	 * 
	 * @param userName
	 * @return applicationId
	 * @throws Exception
	 */	
	@ApiOperation(
		    value = "Find Application Id",
		    notes =
		        "Service to find application id based on user name", response = String.class)
	@RequestMapping(value = "/username", method = RequestMethod.GET)
	public @ResponseBody String findByUserName(@ApiParam(value = "userName", required = true) @RequestParam String userName) throws Exception {
		logger.info("Entered findByUserName()");
		Application appln  = applicationRepository.findByUserName(userName);
		String applicationId = appln.getApplicationId();
		return applicationId;
	}
	
	
}
