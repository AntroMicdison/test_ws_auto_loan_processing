package com.ezb.alp.service;


import com.ezb.alp.Application;
import com.ezb.alp.model.LoanApplication;

public interface ILoanApplicationService {
	/**
	 * Service for Save Loan Application Details
	 * 
	 * @param application
	 * @return
	 * @throws Exception
	 */
	public Application saveLoanApplicationDetails(LoanApplication loanApplication) throws Exception;
}
