package com.ezb.alp.service;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.ezb.alp.Application;
import com.ezb.alp.User;
import com.ezb.alp.model.LoanApplication;
import com.ezb.alp.repository.ApplicationRepository;
import com.ezb.alp.repository.UserRepository;

@Service
public class LoanApplicationService implements ILoanApplicationService {

	@Autowired
	private ApplicationRepository repository;

	@Autowired
	private UserRepository userRepository;

	/**
	 * 14 Digit Account Number Generation
	 * 
	 * @param lastno
	 * @return
	 */
	private String generateAccountNumber(String lastno) {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		String month = String.format("%02d", Calendar.getInstance().get(Calendar.MONTH) + 1);
		if (lastno != null && lastno.length() > 0) {
			long newnum = Long.valueOf(lastno.substring(6, lastno.length())) + 1;
			return year + month + String.format("%08d", newnum);
		} else {
			return year + month + String.format("%08d", 1);
		}

	}

	/**
	 * Service for Save Loan Application Details
	 * 
	 * @return application
	 */
	public Application saveLoanApplicationDetails(LoanApplication loanApplication) throws Exception {

		User user = userRepository.insert(loanApplication.getUser());

		Application application = loanApplication.getApplication();

		Application maxAppId = repository.findTopByOrderByApplicationIdDesc();

		String appId = null;
		if (maxAppId != null) {
			appId = generateAccountNumber(maxAppId.getApplicationId());
		} else {
			appId = generateAccountNumber(null);
		}

		application.setApplicationId(appId);
		application.setUser(user);

		return repository.insert(application);
	}

}
