package com.ezb.alp.model;

import com.ezb.alp.Application;
import com.ezb.alp.User;

public class LoanApplication {
	private Application application;
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

}
