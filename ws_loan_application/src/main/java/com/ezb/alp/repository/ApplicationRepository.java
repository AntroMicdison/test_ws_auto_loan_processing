package com.ezb.alp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ezb.alp.Application;

@Repository
public interface ApplicationRepository extends MongoRepository<Application, String> {

	Application findTopByOrderByApplicationIdDesc();
	
	Application findByUserName(@Param("userName") String userName);

}