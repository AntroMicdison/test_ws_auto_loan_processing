package com.ezb.alp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.ezb.alp.VehicleDetail;

@Repository
public interface VehicleDetailRepository extends MongoRepository<VehicleDetail, String> {

	List<VehicleDetail> findByVehicleMake(@Param("vehicleMake") String vehicleMake);

}