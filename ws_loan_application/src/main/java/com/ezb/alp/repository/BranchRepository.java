package com.ezb.alp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ezb.alp.Branch;

@Repository
public interface BranchRepository extends MongoRepository<Branch, String> {

	Branch findByZipCode(@Param("zipCode") String zipCode);

}