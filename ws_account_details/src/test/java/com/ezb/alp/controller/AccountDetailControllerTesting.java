package com.ezb.alp.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.ezb.alp.Application;
import com.ezb.alp.Loan;
import com.ezb.alp.service.AccountDetailService;

import gherkin.deps.com.google.gson.Gson;

public class AccountDetailControllerTesting {

	private MockMvc mockMvc;

	@Mock
	private AccountDetailService accountDetailService;

	@InjectMocks
	private AccountDetailController accountDetailController;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(accountDetailController).build();
	}

	@Test
	public void testFindAccountDetail() throws Exception {
		String applicationId = "786";
		Application app = new Application();
		app.setApplicationId("786");

		Mockito.when(accountDetailService.getAccountDetails(app.getApplicationId())).thenReturn(app);

		mockMvc.perform(get("/accountdetail?applicationId=" + applicationId).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(jsonPath("applicationId", is(applicationId)));
	}

	@Test
	public void testSaveAccountDetails() throws Exception {

		Application app = new Application();
		app.setApplicationId("786");

		Loan loan = new Loan();
		loan.setRequestedAmt(35000);

		Gson gson = new Gson();

		String json = gson.toJson(app);

		Mockito.when(accountDetailService.saveAccountDetails(any(Application.class))).thenReturn(loan);

		mockMvc.perform(post("/createaccountdetails")
				.contentType(MediaType.parseMediaType("application/json;charset=UTF-8")).content(json))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(MockMvcResultMatchers.jsonPath("requestedAmt", is(35000)));
	}
}
