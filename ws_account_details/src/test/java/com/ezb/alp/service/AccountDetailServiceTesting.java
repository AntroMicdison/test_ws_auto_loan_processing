package com.ezb.alp.service;

import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.ezb.alp.Application;
import com.ezb.alp.Loan;
import com.ezb.alp.repository.AccountDetailRepository;
import com.ezb.alp.repository.LoanRepository;

@RunWith(MockitoJUnitRunner.class)
public class AccountDetailServiceTesting {

	@Mock
	private AccountDetailRepository accountDetailRepository;

	@Mock
	private LoanRepository loanRepository;

	@InjectMocks
	AccountDetailService accountDetailService;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testsaveAccountDetails() throws Exception {
		Application app = new Application();
		app.setApplicationId("786");
		app.setUid(123);
		app.setUserName("JohnBecker");
		app.setRequestedAmt(350000);
		app.setLoanTerm(3);
		app.setLoanYearPeriod(3);
		app.setApplicationState("Submitted");

		double interestamount = (13 / 100) * app.getRequestedAmt();
		int oneyearinterest = (int) Math.round(interestamount);
		int totalpaymentamount = (oneyearinterest * app.getLoanTerm()) + app.getRequestedAmt();
		int totalmonths = app.getLoanTerm() * 12;
		int monthlydueamount = totalpaymentamount / totalmonths;
		Date loanenddate = DateUtils.addMonths(new Date(), totalmonths);
		Date nextduedate = DateUtils.addMonths(new Date(), 1);

		Loan loan = new Loan();
		loan.setRequestedAmt(app.getRequestedAmt());
		loan.setLoanYearPeriod(app.getLoanYearPeriod());
		loan.setLoanTerm(app.getLoanTerm());
		loan.setLoanstartdate(new Date());
		loan.setEmi(monthlydueamount);
		loan.setLoanEndDate(loanenddate);
		loan.setPaymentDueDate(nextduedate);
		loan.setCurrentPaymentDue(monthlydueamount);
		loan.setTotalPaymentDue(totalpaymentamount - monthlydueamount);
		loan.setReceivedOn(new Date());
		loan.setMaturityDate(loanenddate);
		loan.setPayoffAmount(monthlydueamount);
		loan.setInterestPaid((int) Math.round(interestamount));

		Mockito.when(accountDetailRepository.findByApplicationId(app.getApplicationId())).thenReturn(app);

		Mockito.when(loanRepository.insert(loan)).thenReturn(loan);

		Mockito.when(accountDetailService.saveAccountDetails(app)).thenReturn(loan);

	}

	@Test
	public void testGetAccountDetails() throws Exception {
		Application app = new Application();
		app.setApplicationId("786");
		app.setUid(123);
		app.setUserName("JohnBecker");
		app.setRequestedAmt(350000);
		app.setLoanTerm(3);
		app.setLoanYearPeriod(3);
		app.setApplicationState("Submitted");

		Mockito.when(accountDetailRepository.findByApplicationId(app.getApplicationId())).thenReturn(app);

		Application application = accountDetailService.getAccountDetails(app.getApplicationId());

		assertNotNull(application);
	}

}
