package com.ezb.alp.controller

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import spock.lang.Specification

import com.ezb.alp.Application
import com.ezb.alp.Loan
import com.ezb.alp.repository.LoanRepository
import com.ezb.alp.service.AccountDetailService

class AccountDetailControllerTest  extends Specification {
	
	
	/**
	 * AccountDetailController Declaration
	 */
	private AccountDetailController accountDetailController;
	
	/**
	 * AccountDetailService Declaration
	 */
	private AccountDetailService accountDetailService;
	
	/**
	 * Application Entity Declaration
	 */
	private Application application;
	
	/**
	 * Loan Entity Declaration
	 */
	private Loan loan;

	private double interestamount;

	private int oneyearinterest=0;

	private int totalpaymentamount=0;

	private int totalmonths=0;

	private int monthlydueamount=0;

	private Date loanenddate;

	private Date nextduedate;

	
	/**
	 * Setting the User entity values across the class
	 * @return
	 */
	def setup() {
		application = new Application();
		application.setApplicationId("20160800000001");
		application.setRequestedAmt(250000);
		application.setLoanTerm(3);
		loan = new Loan();
		interestamount = (13/100) * application.getRequestedAmt();
		oneyearinterest = (int) Math.round(interestamount);
		totalpaymentamount = (oneyearinterest * application.getLoanTerm())+ application.getRequestedAmt();
		totalmonths = application.getLoanTerm() * 12;
		monthlydueamount = totalpaymentamount/totalmonths;
		loanenddate = DateUtils.addMonths(new Date(), totalmonths);
		nextduedate = DateUtils.addMonths(new Date(), 1);
		accountDetailService = Mock();
		accountDetailController = new AccountDetailController(accountDetailService:accountDetailService);
	}
	
	
	
	def 'find accountDetail by given applicationId'() {
		given:"To Get AccountDetail By Given ApplicationId "
			String applicationId = "20160800000001";
		when:"Identify The Given ApplicationId Is Valid Or Not"
			if(applicationId!=" "){
				accountDetailController.findAccountDetail(applicationId)
			}
		then:"Retrieve AccountDetail As Per The Given Valid ApplicationId"
			  accountDetailService.getAccountDetails(applicationId) >>  application;
			
	}
	
	def 'save accountDetail by given application object'() {
		given:"To Save AccountDetail By Given Application Object"
			loan.setApplication(application);
			loan.setRequestedAmt(application.getRequestedAmt());
			loan.setLoanYearPeriod(application.getLoanYearPeriod());
			loan.setLoanTerm(application.getLoanTerm());
			loan.setLoanstartdate(new Date());
			loan.setEmi(monthlydueamount);
			loan.setLoanEndDate(loanenddate);
			loan.setPaymentDueDate(nextduedate);
			loan.setCurrentPaymentDue(monthlydueamount);
			loan.setTotalPaymentDue(totalpaymentamount-monthlydueamount);
			loan.setReceivedOn(new Date());
			loan.setMaturityDate(loanenddate);
			loan.setPayoffAmount(monthlydueamount);
			loan.setInterestPaid((int) Math.round(interestamount));
		when:"Pass The Application Object"
			if(application!=null){
				accountDetailController.saveAccountDetails(application)
			}
		then:"Save The Loan Details"
			 accountDetailService.saveAccountDetails(application) >>  loan;
	}
	
	
	

}
