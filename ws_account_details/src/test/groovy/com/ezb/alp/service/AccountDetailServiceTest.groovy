package com.ezb.alp.service;

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertNotSame
import static org.junit.Assert.assertNull

import org.apache.commons.lang3.time.DateUtils;

import spock.lang.Specification

import com.ezb.alp.Application
import com.ezb.alp.Loan
import com.ezb.alp.repository.AccountDetailRepository
import com.ezb.alp.repository.LoanRepository


/**
 * TDD test using Spock Framework
 * Class used to test AccountDetailService methods
 * @author Jerry Rydere 
 *
 */
class AccountDetailServiceTest  extends Specification {

	/**
	 * AccountDetailService Declaration
	 */
	private AccountDetailService accountDetailService;
	
	/**
	 * AccountDetailService Declaration For LoanRepositoy
	 */
	private AccountDetailService accountDetailServiceForLoanRepo;

	/**
	 * Application Entity Declaration
	 */
	private Application application;
	
	/**
	 * AccountDetailRepository Declaration
	 */
	private AccountDetailRepository accountDetailRepository;
	
	/**
	 * LoanRepository Declaration
	 */
	private LoanRepository loanRepository;

	/**
	 * Loan Entity Declaration
	 */
	private Loan loan;

	private double interestamount;

	private int oneyearinterest=0;

	private int totalpaymentamount=0;

	private int totalmonths=0;

	private int monthlydueamount=0;

	private Date loanenddate;

	private Date nextduedate;

	/**
	 * Setting the User entity values across the class
	 * @return
	 */
	def setup() {
		application = new Application();
		
		loan = new Loan();
		application.setApplicationId("20160800000001");
		application.setRequestedAmt(250000);
		application.setLoanTerm(3);
		// Calculation Logic
		interestamount = (13/100) * application.getRequestedAmt();
		oneyearinterest = (int) Math.round(interestamount);
		totalpaymentamount = (oneyearinterest * application.getLoanTerm())+ application.getRequestedAmt();
		totalmonths = application.getLoanTerm() * 12;
		monthlydueamount = totalpaymentamount/totalmonths;
		loanenddate = DateUtils.addMonths(new Date(), totalmonths);
		nextduedate = DateUtils.addMonths(new Date(), 1);
		loanRepository = Mock();
		accountDetailServiceForLoanRepo = new AccountDetailService(loanRepository:loanRepository);
		accountDetailRepository = Mock();
		accountDetailService = new AccountDetailService(accountDetailRepository:accountDetailRepository);
		
	}

	def 'find accountDetail by given applicationId'() {
		given:"To Get AccountDetail By Given ApplicationId "
			String applicationId = "20160800000001";
		when:"Identify The Given ApplicationId Is Valid Or Not"
			if(applicationId!=" "){
				accountDetailService.getAccountDetails(applicationId)
			}
		then:"Retrieve AccountDetail As Per The Given Valid ApplicationId"
			 accountDetailRepository.findByApplicationId(applicationId) >>  application;
	}
	
	
	def 'find accountDetail by Invalid applicationId'() {
		given:"To Get AccountDetail By Given ApplicationId "
			String applicationId = " ";
		when:"Identify The Given ApplicationId Is Valid Or Not"
			if(applicationId!=" "){
				accountDetailService.getAccountDetails(applicationId)
			}
		then:"Invalid ApplicationId"
			accountDetailRepository.findByApplicationId(applicationId)  >>  new Application(applicationId:applicationId);
			assertNotSame("20160800000001", applicationId);
	}

	def 'save accountDetail by given application object'() {
		given:"To Save AccountDetail By Given Application Object"
			
		    Application appl = new Application();
			appl.setApplicationId("20160800000001");
			appl.setRequestedAmt(12345);
			appl.setLoanYearPeriod(3);
			appl.setLoanTerm(1);
			loan.setApplication(appl);
			loan.setRequestedAmt(appl.getRequestedAmt());
			loan.setLoanYearPeriod(appl.getLoanYearPeriod());
			loan.setLoanTerm(appl.getLoanTerm());
			loan.setLoanstartdate(new Date());
			loan.setEmi(monthlydueamount);
			loan.setLoanEndDate(loanenddate);
			loan.setPaymentDueDate(nextduedate);
			loan.setCurrentPaymentDue(monthlydueamount);
			loan.setTotalPaymentDue(totalpaymentamount-monthlydueamount);
			loan.setReceivedOn(new Date());
			loan.setMaturityDate(loanenddate);
			loan.setPayoffAmount(monthlydueamount);
			loan.setInterestPaid((int) Math.round(interestamount));
		
			/*when:"Identify The Given ApplicationId Is Valid Or Not"
				accountDetailService.getAccountDetails(appl.getApplicationId())
			then:"Retrieve AccountDetail As Per The Given Valid ApplicationId"
				accountDetailRepository.findByApplicationId(appl.getApplicationId()) >>  appl;*/
			when:"Get The Application Object"
				loanRepository.insert(loan);
			then:"Save The Loan Details"
				accountDetailServiceForLoanRepo.saveAccountDetails(appl) >> loan;
	}
	
	def 'save accountDetail by passing null object'() {
		given:"To Save AccountDetail By Given Application Object"
			loan = null;
			application=null;
		when:"Check The Application And Loan Value Object is Not Null"
			if(application!=null && loan!=null){
				loanRepository.insert(loan);
			}
		then:"Application And Loan Object Found As NULL Object"
			accountDetailServiceForLoanRepo.saveAccountDetails(application)  >>  loan;
			assertNull(loan);
			assertNull(application);
	}
	
	def 'calculate loan logic by given application object'() {
		given:"Calculation logic for Loan Using Application Object"
			Application appl = new Application();
			appl.setApplicationId("20160800000001");
			appl.setRequestedAmt(12345);
			appl.setLoanYearPeriod(3);
			appl.setLoanTerm(1);
			loan.setApplication(appl);
			loan.setLoanstartdate(new Date());
			loan.setEmi(monthlydueamount);
			loan.setLoanEndDate(loanenddate);
			loan.setPaymentDueDate(nextduedate);
			loan.setCurrentPaymentDue(monthlydueamount);
			loan.setTotalPaymentDue(totalpaymentamount-monthlydueamount);
			loan.setReceivedOn(new Date());
			loan.setMaturityDate(loanenddate);
			loan.setPayoffAmount(monthlydueamount);
			loan.setInterestPaid((int) Math.round(interestamount));
			loan.setRequestedAmt(appl.getRequestedAmt());
			loan.setLoanYearPeriod(appl.getLoanYearPeriod());
			loan.setLoanTerm(appl.getLoanTerm());
		when: "Calculate Split Logic"
			accountDetailService.calculateLoan(appl);
		then:"Calculation Completed Successfully"
			accountDetailRepository.findByApplicationId(appl.getApplicationId()) >>  application;
	}	
		
}
