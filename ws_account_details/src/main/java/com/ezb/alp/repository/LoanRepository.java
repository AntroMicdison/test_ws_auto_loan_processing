package com.ezb.alp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ezb.alp.Loan;




/**
 * 
 * @author Jerry Rydere
 *
 */
@Repository
public interface LoanRepository extends MongoRepository<Loan, String> {
}