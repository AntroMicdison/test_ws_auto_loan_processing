package com.ezb.alp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ezb.alp.Application;



/**
 * This interface is used to declare Find Application Id
 * @author Jerry Rydere
 *
 */
@Repository
public interface AccountDetailRepository extends MongoRepository<Application, Integer> {

	Application findByApplicationId(@Param("applicationId") String applicationId);
	
}
