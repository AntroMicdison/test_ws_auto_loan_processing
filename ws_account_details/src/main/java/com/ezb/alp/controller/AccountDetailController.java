package com.ezb.alp.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ezb.alp.Application;
import com.ezb.alp.Loan;
import com.ezb.alp.service.AccountDetailService;

/**
 * This Controller is used to route to the methods 
 * findAccountDetail
 * saveAccountDetails
 * @author Jerry Rydere
 *
 */
@RestController
@RequestMapping("/")
public class AccountDetailController {
	/**
	 * Autowiring accountDetailService
	 */
	@Autowired
	private AccountDetailService accountDetailService;

	/**
	 * Service for Find Account Details
	 * 
	 * @param applicationId
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(
		    value = "Fetch Loan Application Details",
		    notes =
		        "Service for fetch the Loan Application Status and Details",
		    response = Application.class)
	@RequestMapping(value = "/accountdetail", method = RequestMethod.GET)
	public Application findAccountDetail(@ApiParam(value = "Application ID", required = true) @RequestParam String applicationId) throws Exception {
		return accountDetailService.getAccountDetails(applicationId);
	}
	
	/**
	 * Service for Save/Create Account Details
	 * 
	 * @param application
	 * @return
	 * @throws Exception
	 */
	
	@ApiOperation(
		    value = "Account Details Creation",
		    notes =
		        "Service for Account Details Creation",
		    response = Loan.class)
	@RequestMapping(value = "/createaccountdetails", method = RequestMethod.POST)
	public @ResponseBody Loan saveAccountDetails(@ApiParam(value = "Application Details", required = true) @RequestBody Application application) throws Exception {
		Loan loan = null;
		loan = accountDetailService.saveAccountDetails(application);
		return loan;
	}
}
