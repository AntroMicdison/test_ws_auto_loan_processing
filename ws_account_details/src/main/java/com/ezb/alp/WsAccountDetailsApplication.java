package com.ezb.alp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * This class is used to execute the Application
 * 
 * @author Jerry Rydere
 *
 */
@SpringBootApplication
@EnableSwagger2
// @EnableEurekaClient
public class WsAccountDetailsApplication {
	public static void main(String[] args) {
		SpringApplication.run(WsAccountDetailsApplication.class, args);
	}
	/*
	 * @Bean
	 * 
	 * @Profile("!default") public EurekaInstanceConfigBean
	 * eurekaInstanceConfig(InetUtils inetUtils) { EurekaInstanceConfigBean b =
	 * new EurekaInstanceConfigBean(inetUtils); AmazonInfo info =
	 * AmazonInfo.Builder.newBuilder().autoBuild("eureka");
	 * b.setDataCenterInfo(info); return b; }
	 */

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.ezb.alp.controller")).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		ApiInfo apiInfo = new ApiInfo("Account Details Creation and Fetch Application Status", null, "API TOS",
				"Terms of service", "JerryRyder@cognizant.com", null, null);
		return apiInfo;
	}

	@Bean
	public CorsFilter corsFilter() {

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true); // you USUALLY want this
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("POST");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}
}
