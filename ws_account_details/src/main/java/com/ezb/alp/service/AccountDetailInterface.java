package com.ezb.alp.service;

import com.ezb.alp.Application;
import com.ezb.alp.Loan;

/**
 * This Interface is used to declare Get & Save Account Details
 * @author Jerry Rydere
 *
 */
public interface AccountDetailInterface {
	/**
	 * Get Account Details Based on the Application Id
	 * 
	 * @param applicationId
	 * @return
	 * @throws Exception
	 */
	public Application getAccountDetails(String applicationId) throws Exception;
	
	/**
	 * Save Account Details
	 * 
	 * @param application
	 * @return
	 * @throws Exception
	 */
	public Loan saveAccountDetails(Application application) throws Exception;
	
}
