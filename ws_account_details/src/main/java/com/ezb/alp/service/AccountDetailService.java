package com.ezb.alp.service;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ezb.alp.Application;
import com.ezb.alp.Loan;
import com.ezb.alp.repository.AccountDetailRepository;
import com.ezb.alp.repository.LoanRepository;

/**
 * This Service is used to Get & Save Account Details 
 * @author Jerry Rydere
 *
 */
@Service
public class AccountDetailService implements AccountDetailInterface {

	/**
	 * Autowiring accountDetailRepository
	 */
	@Autowired
	private AccountDetailRepository accountDetailRepository;
	
	/**
	 * Autowiring loanRepository
	 */
	@Autowired
	private LoanRepository loanRepository;
	
	public static final double INTEREST = 13;

	/**
	 * Method used to getAccountDetails
	 * @return application
	 */
	public Application getAccountDetails(String applicationId) throws Exception {
		return accountDetailRepository.findByApplicationId(applicationId);
	}
	
	/**
	 * Mthod used to saveAccountDetails based on Application object
	 * @return loan 
	 */
	public Loan saveAccountDetails(Application application) throws Exception {
		Loan loan = calculateLoan(application);
		return loanRepository.insert(loan);
	}
	
	/**
	 * Method to calculate loan amount based on the given value
	 * @param application
	 * @return
	 * @throws Exception
	 */
	private Loan calculateLoan(Application application) throws Exception{
		double interestamount = (INTEREST/100) * application.getRequestedAmt();		
		int oneyearinterest = (int) Math.round(interestamount);	
		int totalpaymentamount = (oneyearinterest * application.getLoanTerm())+ application.getRequestedAmt();
		int totalmonths = application.getLoanTerm() * 12;
		int monthlydueamount = totalpaymentamount/totalmonths;
		Date loanenddate = DateUtils.addMonths(new Date(), totalmonths);
		Date nextduedate = DateUtils.addMonths(new Date(), 1);
		Application app = getAccountDetails(application.getApplicationId());
		Loan loan = new Loan();
		loan.setApplication(app);
		loan.setRequestedAmt(application.getRequestedAmt());
		loan.setLoanYearPeriod(application.getLoanYearPeriod());
		loan.setLoanTerm(application.getLoanTerm());
		loan.setLoanstartdate(new Date());
		loan.setEmi(monthlydueamount);
		loan.setLoanEndDate(loanenddate);
		loan.setPaymentDueDate(nextduedate);
		loan.setCurrentPaymentDue(monthlydueamount);
		loan.setTotalPaymentDue(totalpaymentamount-monthlydueamount);
		loan.setReceivedOn(new Date());
		loan.setMaturityDate(loanenddate);
		loan.setPayoffAmount(monthlydueamount);
		loan.setInterestPaid((int) Math.round(interestamount));
		return loan;
	
	}
	
}