package com.ezb.alp.service;

import com.ezb.alp.Identity;
import com.ezb.alp.User;

public interface ICustomerService {
	User login(Identity identity);
}