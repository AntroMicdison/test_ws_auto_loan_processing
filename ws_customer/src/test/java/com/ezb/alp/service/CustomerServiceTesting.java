package com.ezb.alp.service;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.ezb.alp.Identity;
import com.ezb.alp.User;
import com.ezb.alp.repository.CustomerRepository;

import gherkin.deps.com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTesting {

	private MockMvc mockMvc;

	@Mock
	private CustomerRepository customerRepository;

	@InjectMocks
	CustomerService customerService;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(customerService).build();
	}

	@Test
	public void testLogin() throws Exception {

		User user = new User();
		user.setUserName("john");

		Identity identity = new Identity();
		identity.setUserName("john");
		identity.setPassword("john");
		identity.setPlatinum(true);
		identity.setUser(user);

		Gson gson = new Gson();

		String json = gson.toJson(identity);

		Mockito.when(customerRepository.findByUserNameAndPassword(identity.getUserName(), identity.getPassword()))
				.thenReturn(identity);

		mockMvc.perform(
				post("/login").contentType(MediaType.parseMediaType("application/json;charset=UTF-8")).content(json))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(MockMvcResultMatchers.jsonPath("userName", is("john")));
	}

}
