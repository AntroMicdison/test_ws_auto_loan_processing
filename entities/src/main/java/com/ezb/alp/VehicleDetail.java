package com.ezb.alp;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "vehicledetail")
public class VehicleDetail {

	@Id
	private String id;
	private Integer vehicleDetailsId;
	private String vehicleType;
	private int vehicleYear;
	private String vehicleMake;
	private String vehicleModel;
	private String vehIdentNo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getVehicleDetailsId() {
		return vehicleDetailsId;
	}

	public void setVehicleDetailsId(Integer vehicleDetailsId) {
		this.vehicleDetailsId = vehicleDetailsId;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public int getVehicleYear() {
		return vehicleYear;
	}

	public void setVehicleYear(int vehicleYear) {
		this.vehicleYear = vehicleYear;
	}

	public String getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getVehIdentNo() {
		return vehIdentNo;
	}

	public void setVehIdentNo(String vehIdentNo) {
		this.vehIdentNo = vehIdentNo;
	}

}
